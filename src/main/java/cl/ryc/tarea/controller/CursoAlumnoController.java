package cl.ryc.tarea.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.ryc.tarea.entity.CursoAlumno;
import cl.ryc.tarea.repository.CursoAlumnoRepository;

@RestController()
@RequestMapping("cursoalumno")
public class CursoAlumnoController {
	
	@Autowired
	CursoAlumnoRepository cursoAlumnoRepo;
	
	@GetMapping(path = "/{alumnoId}", produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CursoAlumno>> listarCursos(@PathVariable("alumnoId") Long alumnoId)
	{
		return new ResponseEntity<List<CursoAlumno>>(cursoAlumnoRepo.findByAlumnoId(alumnoId), HttpStatus.OK); 
	}

	@PostMapping(produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> asigarCurso(@RequestBody CursoAlumno cursoAlumno)
	{
		cursoAlumnoRepo.save(cursoAlumno);
		return new ResponseEntity<Void>(HttpStatus.CREATED); 
	}
	
}