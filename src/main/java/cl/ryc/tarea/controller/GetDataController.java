package cl.ryc.tarea.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.ryc.tarea.repository.CursoAlumnoRepository;
import cl.ryc.tarea.repository.CursoRepository;
import cl.ryc.tarea.repository.UsuarioReposiroty;
import cl.ryc.tarea.entity.CursoAlumno;
import cl.ryc.tarea.entity.NotaAlumno;
import cl.ryc.tarea.entity.Usuario;
import cl.ryc.tarea.model.Alumno;
import cl.ryc.tarea.model.GetData;

@RestController
@RequestMapping("GetData")
public class GetDataController {
	
	 @Autowired
	 CursoRepository cursoRepo;
	 
	 @Autowired
	 UsuarioReposiroty usuarioRepo;
	 
	 @Autowired
	 CursoAlumnoRepository cursoAlumnoRepo;

	@GetMapping(path = "{cursoId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getData(@PathVariable("cursoId") Long cursoId)
	{	
		GetData getData = new GetData();
		List<CursoAlumno> cursosAlumno = new ArrayList<>();
		Usuario usuario = new Usuario();
		List<Alumno> listaAlumnos = new ArrayList<>();
		List<BigDecimal> notas = new ArrayList<>();
		int cantNotas = 0;
		double suma = 0;
		BigDecimal promedio = new BigDecimal(0);
		
		cursosAlumno.addAll(cursoAlumnoRepo.findByCursoId(cursoId));
		
		for (CursoAlumno curso : cursosAlumno) {
			Alumno alumno = new Alumno();
					
			usuario = usuarioRepo.findByIdAndTipoUsuario(curso.getAlumnoId(), (long) 3).get();
			
			alumno.setNombre(usuario.nombreCompleto());
			
			cantNotas = curso.getNotaAlumno().size();
			
			System.out.println("Cant notas: " + cantNotas);
			
			for (NotaAlumno nota : curso.getNotaAlumno()) {
				notas.add(nota.getNota());
				promedio.add(nota.getNota());
				suma = suma + nota.getNota().doubleValue();
			}
			
			alumno.setNotas(notas);
			
			if(cantNotas > 0)
			{
				promedio = new BigDecimal((suma / cantNotas));
			}
				
			
			System.out.println("promedio " + promedio);
			alumno.setPromedio(promedio);
			listaAlumnos.add(alumno);
		}
		
		getData.setNombreCruso(cursoRepo.findById(cursoId).get().getNombre());
		getData.setNombreProfesor(usuarioRepo.findByIdAndTipoUsuario(cursoRepo.findById(cursoId).get().getProfesorId(), (long) 2).get().nombreCompleto());
		getData.setAlumnos(listaAlumnos);
		
		return new ResponseEntity<GetData>(getData, HttpStatus.OK);
	}
	
}
