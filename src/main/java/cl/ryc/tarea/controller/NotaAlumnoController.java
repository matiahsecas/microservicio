package cl.ryc.tarea.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.ryc.tarea.entity.CursoAlumno;
import cl.ryc.tarea.entity.NotaAlumno;
import cl.ryc.tarea.repository.CursoAlumnoRepository;
import cl.ryc.tarea.repository.NotaAlumnoRepository;

@RestController
@RequestMapping("Evaluar")
public class NotaAlumnoController {
	
	@Autowired
	NotaAlumnoRepository notaAlumnoRepo;
	
	@Autowired
	CursoAlumnoRepository cursoAlumnoRepo;
	
	@GetMapping(path = "/{alumnoId}/{cursoId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CursoAlumno> listarNotaAlumno(@PathVariable("alumnoId") Long alumnoId, @PathVariable("cursoId") Long cursoId)
	{
		return new ResponseEntity<CursoAlumno>(cursoAlumnoRepo.findByCursoIdAndAlumnoId(cursoId, alumnoId).get(), HttpStatus.OK); 
	}

	@PostMapping(path = "/{alumnoId}/{cursoId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> evaluar(@RequestBody NotaAlumno notaAlumno, @PathVariable("alumnoId") Long alumnoId, @PathVariable("cursoId") Long cursoId)
	{		
		if(cursoAlumnoRepo.findByCursoIdAndAlumnoId(cursoId, alumnoId).isPresent())
		{
			int cantidadNotas = cursoAlumnoRepo.findByCursoIdAndAlumnoId(cursoId, alumnoId).get().getNotaAlumno().size();
			
			if(cantidadNotas < 6)
			{
				notaAlumnoRepo.save(notaAlumno);
				return new ResponseEntity<Void>(HttpStatus.CREATED); 
			}
				
			else
			{
				return new ResponseEntity<String>("El alumno ya posee la cantidad máxima de notas", HttpStatus.NO_CONTENT);
			}
		}
		
		return new ResponseEntity<String>("Ocurrio un problema al intentar ingresar la nota", HttpStatus.NO_CONTENT); 
	}
}
