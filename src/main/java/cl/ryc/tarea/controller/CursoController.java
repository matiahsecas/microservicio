package cl.ryc.tarea.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.ryc.tarea.entity.Curso;
import cl.ryc.tarea.repository.CursoRepository;

@RestController()
@RequestMapping("/Curso")
public class CursoController {

	@Autowired
	CursoRepository cursoRepository;
	
	@PostMapping(produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> agregarCurso(@RequestBody Curso curso)
	{
		cursoRepository.save(curso);
		return new ResponseEntity<Void>(HttpStatus.CREATED); 
	}
}
