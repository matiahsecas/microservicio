package cl.ryc.tarea.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.ryc.tarea.entity.Usuario;
import cl.ryc.tarea.repository.UsuarioReposiroty;

@RestController()
@RequestMapping("/Alumno")
public class AlumnoController {

	@Autowired
	UsuarioReposiroty usuarioRepository;
	
	@GetMapping(path = "prueba")
	public String prueba()
	{
		return "Hola mundo";
	}
	
	@PostMapping(produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> agregarAlumno(@RequestBody Usuario usuario)
	{
		usuario.setTipoUsuario((long) 3);
		
		System.out.println(usuario.toString());
		
		usuarioRepository.save(usuario);
		return new ResponseEntity<Void>(HttpStatus.CREATED); 
	}
	
	@GetMapping(produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarAlumnos()
	{
		return new ResponseEntity<>(usuarioRepository.findByTipoUsuario((long) 3), HttpStatus.OK); 
	}
}
