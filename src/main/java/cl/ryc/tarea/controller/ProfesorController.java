package cl.ryc.tarea.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.ryc.tarea.entity.Curso;
import cl.ryc.tarea.entity.Usuario;
import cl.ryc.tarea.repository.NotaAlumnoRepository;
import cl.ryc.tarea.repository.UsuarioReposiroty;

@RestController()
@RequestMapping("/Profesor")
public class ProfesorController {

	@Autowired
	UsuarioReposiroty usuarioRepository;
	
	@Autowired
	NotaAlumnoRepository notaAlumnoRepo;
	
	@PostMapping(path = "profesor", produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> agregarProfesor(@RequestBody Usuario usuario)
	{
		usuario.setTipoUsuario((long) 2);		
		usuarioRepository.save(usuario);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	@GetMapping(path = "/obtenerCursos/{profesorId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarCursosProfesor(@PathVariable("profesorId") Long profesorId)
	{
		if(usuarioRepository.findByIdAndTipoUsuario(profesorId,(long) 2).isPresent())
			return new ResponseEntity<List<Curso>>(usuarioRepository.findByIdAndTipoUsuario(profesorId,(long) 2).get().getCursos(), HttpStatus.OK);
		
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping(produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarProfesores()
	{
		return new ResponseEntity<>(usuarioRepository.findByTipoUsuario((long) 2), HttpStatus.OK); 
	}
	
}
