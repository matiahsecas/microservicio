package cl.ryc.tarea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class TareaMicroservicioApplication {

	public static void main(String[] args) {
		SpringApplication.run(TareaMicroservicioApplication.class, args);
	}

}
