package cl.ryc.tarea.model;

import java.util.List;

public class GetData {

	private String nombreCruso;
	private String nombreProfesor;
	private List<Alumno> alumnos;

	public String getNombreCruso() {
		return nombreCruso;
	}

	public void setNombreCruso(String nombreCruso) {
		this.nombreCruso = nombreCruso;
	}

	public String getNombreProfesor() {
		return nombreProfesor;
	}

	public void setNombreProfesor(String nombreProfesor) {
		this.nombreProfesor = nombreProfesor;
	}

	public List<Alumno> getAlumnos() {
		return alumnos;
	}

	public void setAlumnos(List<Alumno> alumnos) {
		this.alumnos = alumnos;
	}	
	
}
