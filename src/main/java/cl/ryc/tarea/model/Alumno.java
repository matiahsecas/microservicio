package cl.ryc.tarea.model;

import java.math.BigDecimal;
import java.util.List;

public class Alumno {
	
	private String nombre;
	private BigDecimal promedio;
	private List<BigDecimal> notas;
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public BigDecimal getPromedio() {
		return promedio;
	}
	
	public void setPromedio(BigDecimal promedio) {
		this.promedio = promedio;
	}
	
	public List<BigDecimal> getNotas() {
		return notas;
	}
	
	public void setNotas(List<BigDecimal> notas) {
		this.notas = notas;
	}
	
}
