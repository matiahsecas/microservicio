package cl.ryc.tarea.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.ryc.tarea.entity.CursoAlumno;

@Repository
public interface CursoAlumnoRepository extends JpaRepository<CursoAlumno, Long> {

	public List<CursoAlumno> findByAlumnoId(Long alumnoId);
	public List<CursoAlumno> findByCursoId(Long cursoId);
	public Optional<CursoAlumno> findById(Long id);
	public Optional<CursoAlumno> findByCursoIdAndAlumnoId(Long cursoId, Long alumnoId);
	
}
