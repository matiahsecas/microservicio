package cl.ryc.tarea.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.ryc.tarea.entity.NotaAlumno;

@Repository
public interface NotaAlumnoRepository extends JpaRepository<NotaAlumno, Long> {
	public List<NotaAlumno> findByCursoAlumno(Long cursoAlumno);
}
