package cl.ryc.tarea.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.ryc.tarea.entity.Curso;

@Repository
public interface CursoRepository extends JpaRepository<Curso, Long> {

	public Optional<Curso> findById(Long id);
	public List<Curso> findByProfesorId(Long profesorId);
	
}
