package cl.ryc.tarea.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "NOTA_ALUMNO")
public class NotaAlumno implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "NOTA_ALUMNO_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_Sequence")
	@SequenceGenerator(name = "id_Sequence", sequenceName = "NOTA_ALUMNO_SEQ", allocationSize=1)
	private Long id;
	
	@Column(name = "NOTA")
	private BigDecimal nota;
	
	@Column(name = "CURSO_ALUMNO_ID")
	private Long cursoAlumno;
	
	public NotaAlumno()
	{
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getNota() {
		return nota;
	}

	public void setNota(BigDecimal nota) {
		this.nota = nota;
	}

	public Long getCursoAlumno() {
		return cursoAlumno;
	}

	public void setCursoAlumno(Long cursoAlumno) {
		this.cursoAlumno = cursoAlumno;
	}
	
}
