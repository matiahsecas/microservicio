package cl.ryc.tarea.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "USUARIO")
public class Usuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "USUARIO_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_Sequence")
	@SequenceGenerator(name = "id_Sequence", sequenceName = "USUARIO_SEQ", allocationSize=1)
	private Long id;
	
	@Column(name = "USUARIO_NOMBRE")
	private String nombre;
	
	@Column(name = "USUARIO_APELLIDO_PATERNO")
	private String apellidoPaterno;
	
	@Column(name = "USUARIO_APELLIDO_MATERNO")
	private String aplellidoMaterno;
	
	@Column(name = "USUARIO_RUT")
	private String rut;
	
	@Column(name = "TIPO_USUARIO_ID")
	private Long tipoUsuario;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "PROFESOR_ID", referencedColumnName = "USUARIO_ID")
	private List<Curso> cursos;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "ALUMNO_ID", referencedColumnName = "USUARIO_ID")
	private List<CursoAlumno> cursosAlumno;
	
	/*@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="CURSO_ALUMNO",
			joinColumns = @JoinColumn(name="ALUMNO_ID", referencedColumnName = "USUARIO_ID"),
			inverseJoinColumns = @JoinColumn(name="CURSO_ID", referencedColumnName = "CURSO_ID"))
	private Set<CursoAlumno> alumnoCursos = new HashSet<CursoAlumno>();*/
	
	public Usuario()
	{
		
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellidoPaterno() {
		return apellidoPaterno;
	}


	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}


	public String getAplellidoMaterno() {
		return aplellidoMaterno;
	}


	public void setAplellidoMaterno(String aplellidoMaterno) {
		this.aplellidoMaterno = aplellidoMaterno;
	}


	public String getRut() {
		return rut;
	}


	public void setRut(String rut) {
		this.rut = rut;
	}


	public Long getTipoUsuario() {
		return tipoUsuario;
	}


	public void setTipoUsuario(Long tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

	public List<CursoAlumno> getCursosAlumno() {
		return cursosAlumno;
	}

	public void setCursosAlumno(List<CursoAlumno> cursosAlumno) {
		this.cursosAlumno = cursosAlumno;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nombre=" + nombre + ", apellidoPaterno=" + apellidoPaterno
				+ ", aplellidoMaterno=" + aplellidoMaterno + ", rut=" + rut + ", tipoUsuario=" + tipoUsuario + "]";
	}
	
	public String nombreCompleto()
	{
		return this.nombre + " " + this.apellidoPaterno + " " + this.aplellidoMaterno;
	}
	
}
