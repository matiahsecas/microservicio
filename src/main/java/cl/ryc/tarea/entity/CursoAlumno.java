package cl.ryc.tarea.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CURSO_ALUMNO")
public class CursoAlumno implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CURSO_ALUMNO_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_Sequence")
	@SequenceGenerator(name = "id_Sequence", sequenceName = "CURSO_ALUMNO_SEQ", allocationSize=1)
	private Long id;
	
	@Column(name = "CURSO_ID")
	private Long cursoId;
	
	@Column(name = "ALUMNO_ID")
	private Long alumnoId;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "CURSO_ALUMNO_ID", referencedColumnName = "CURSO_ALUMNO_ID")
	private List<NotaAlumno> notaAlumno;
	
	public CursoAlumno()
	{
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCursoId() {
		return cursoId;
	}

	public void setCursoId(Long cursoId) {
		this.cursoId = cursoId;
	}

	public Long getAlumnoId() {
		return alumnoId;
	}

	public void setAlumnoId(Long alumnoId) {
		this.alumnoId = alumnoId;
	}

	public List<NotaAlumno> getNotaAlumno() {
		return notaAlumno;
	}

	public void setNotaAlumno(List<NotaAlumno> notaAlumno) {
		this.notaAlumno = notaAlumno;
	}

	@Override
	public String toString() {
		return "CursoAlumno [id=" + id + ", cursoId=" + cursoId + ", alumnoId=" + alumnoId + ", notaAlumno="
				+ notaAlumno + "]";
	}
	
}
